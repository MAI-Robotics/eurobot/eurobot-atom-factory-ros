#!/usr/bin/env python
import math
import rospy
from controller.msg import Joystick
from controller.msg import Arm_absolute
from controller.msg import Arm_relative
from controller.msg import Arm_height
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool
from std_msgs.msg import Int8
from std_msgs.msg import UInt8
from std_msgs.msg import Int16
from std_msgs.msg import UInt16
from controller.msg import drive
import sys
import time

global actPub, team

def joystickCB(data):
    global actPub, team
    if data.buttons[7] == 1 and team != 0:
        actPub.publish(Int8(team))
        team = 0

def main():
    global actPub, team
    team = 1
    rospy.init_node('PurpleAct')
    actPub = rospy.Publisher('activate', Int8, queue_size=1)
    rospy.Subscriber("joy", Joy, joystickCB)
    rospy.spin()

if __name__ == '__main__':
    main()